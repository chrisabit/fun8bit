# fun8bit
Just playing with prehistoric components from the 8bit era. cpus like 6502, z80, soundchips, roms, rams, etc. 
For fun only... It does not make any sense ;-)

## rom-reader
ESP32 based ROM/PROM/EPROM Reader:
* ESP32 addresses ROM via cascaded 2 * 8 Bit TTL 74HC595 Shift Registers
* 3.3V / 5V level converter
* ROM data output (EPROM < 4V !) is amplified by a TTL 74LS245 (Bus Transceiver)
* 8 Bit Input is read by GPIO

![rom-reader-breadboard](images/rom-reader-breadboard.jpg)

Software provides Read / Data-Transfer by the best Web-App the universe has ever seen :)

## ram-reader
Nobody needs more than 2k of (static) RAM. Nobody! This VIC20 like RAM can be checked by the ram-reader. Similar setup as the rom-reader but with Write-UseCases.

## more fun stuff
_...hic sunt leones..._

