#include <WiFi.h>
#include <AsyncTCP.h>
#include "ESPAsyncWebServer.h"

const int PIN_ENABLE = 4;
const int PIN_OUTPUT = 5;

const int PIN_LATCH = 32;
const int PIN_CLOCK = 25;
const int PIN_DATA = 27;

const int PIN_BIT_0 = 16;
const int PIN_BIT_1 = 17;
const int PIN_BIT_2 = 21;
const int PIN_BIT_3 = 22;
const int PIN_BIT_4 = 23;
const int PIN_BIT_5 = 19;
const int PIN_BIT_6 = 18;
const int PIN_BIT_7 = 26;


const int ROM_SIZE = 0x2000;

const char* ssid     = "ssid";
const char* password = "pwd";

AsyncWebServer server(80);

uint8_t buffer[ROM_SIZE];

volatile uint8_t semaphore = 0;

PROGMEM uint16_t addr = -1;

hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

void handleRoot(AsyncWebServerRequest *request) {
  String message = "<html><head></head><body style='font-family: sans-serif; font-size: 12px'>Following functions are available:<br><br>";
  message += "<a href='/read'>/read</a> start read rom<br>";
  message += "<a href='/data'>/data</a> get data<br>";
  request->send(200, "text/html", message);
}

void handleNotFound(AsyncWebServerRequest *request){
    request->send(404, "text/plain", "Not found");
}

void IRAM_ATTR on_time() {
  portENTER_CRITICAL_ISR(&timerMux);
  semaphore++;
  portEXIT_CRITICAL_ISR(&timerMux);
}

void init_timer(uint64_t microseconds) {
  // 80000000 / 80 = 1000000 tics / second
  timer = timerBegin(0, 80, true);                
  timerAttachInterrupt(timer, &on_time, true);    
  
  timerAlarmWrite(timer, microseconds, true);           
  timerAlarmEnable(timer);
}

void handleData(AsyncWebServerRequest *request) {
  request->send_P(200, "application/octet-stream", buffer, ROM_SIZE, NULL);
}

inline boolean is_reading() {
  return addr >= 0 && addr < ROM_SIZE;
}

inline uint8_t read_input() {
  uint8_t input = B00000000;

  if (digitalRead(PIN_BIT_0) == HIGH) {
    input = input | B00000001;
  }
  if (digitalRead(PIN_BIT_1) == HIGH) {
    input = input | B00000010;
  }
  if (digitalRead(PIN_BIT_2) == HIGH) {
    input = input | B00000100;
  }
  if (digitalRead(PIN_BIT_3) == HIGH) {
    input = input | B00001000;
  }
  if (digitalRead(PIN_BIT_4) == HIGH) {
    input = input | B00010000;
  }
  if (digitalRead(PIN_BIT_5) == HIGH) {
    input = input | B00100000;
  }
  if (digitalRead(PIN_BIT_6) == HIGH) {
    input = input | B01000000;
  }
  if (digitalRead(PIN_BIT_7) == HIGH) {
    input = input | B10000000;
  }
  
  return input;
}

void setup() {
  Serial.begin(115200);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  delay(500);
  int intRetry = 0;
  while ((WiFi.status() != WL_CONNECTED) && (intRetry < 20)){
    Serial.print(".");
    intRetry++;
    delay(500);
  }

  if (WiFi.status() == WL_CONNECTED) {
    Serial.print("IP Address: ");
    Serial.println(WiFi.localIP());
  
    server.on("/", handleRoot);
    server.on("/read", [](AsyncWebServerRequest *request){
      Serial.println("handle /readrom");
      if (!is_reading()) {
        request->send(200, "text/plain", "ok");
        addr = 0;
      }
      else {
        request->send(406, "text/plain", "reading");
      }
    });
  
    server.on("/data", [](AsyncWebServerRequest *request){
      Serial.println("handle /data");
      if (is_reading()) {
        request->send(406, "text/plain", "reading");
      }
      handleData(request);
    });
    
    server.onNotFound(handleNotFound);
    server.begin();
  }

  init_timer(10);
  pinMode(PIN_ENABLE, OUTPUT);
  pinMode(PIN_OUTPUT, OUTPUT);
  digitalWrite(PIN_ENABLE, HIGH);
  digitalWrite(PIN_OUTPUT, HIGH);

  pinMode(PIN_LATCH, OUTPUT);
  pinMode(PIN_CLOCK, OUTPUT);
  pinMode(PIN_DATA, OUTPUT);

  pinMode(PIN_BIT_0, INPUT);
  pinMode(PIN_BIT_1, INPUT);
  pinMode(PIN_BIT_2, INPUT);
  pinMode(PIN_BIT_3, INPUT);
  pinMode(PIN_BIT_4, INPUT);
  pinMode(PIN_BIT_5, INPUT);
  pinMode(PIN_BIT_6, INPUT);
  pinMode(PIN_BIT_7, INPUT);
}

void read() {
  if (addr == 0) {
    Serial.println("Start reading...");
  }
  uint8_t low = addr & 0x00FF;
  uint8_t high = (addr & 0xFF00) >> 8;

  digitalWrite(PIN_LATCH, LOW);
  shiftOut(PIN_DATA, PIN_CLOCK, MSBFIRST, high);
  shiftOut(PIN_DATA, PIN_CLOCK, MSBFIRST, low);

  digitalWrite(PIN_ENABLE, LOW);
  delayMicroseconds(1);
  digitalWrite(PIN_OUTPUT, LOW);
  delayMicroseconds(1);
 
  digitalWrite(PIN_LATCH, HIGH);

  uint8_t data = read_input();
  buffer[addr] = data;
  addr++;

  digitalWrite(PIN_OUTPUT, HIGH);
  delayMicroseconds(1);
  digitalWrite(PIN_ENABLE, HIGH);

  if (addr == ROM_SIZE) {
    addr = -1;
    Serial.println("...reading done!");
  }
}

void loop() {
  if (is_reading()) {
    if (semaphore > 0) {
      portENTER_CRITICAL(&timerMux);
      semaphore--;
      portEXIT_CRITICAL(&timerMux);
      read();
    }
  }
}
 