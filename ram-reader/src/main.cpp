#include <WiFi.h>
#include <AsyncTCP.h>
#include "ESPAsyncWebServer.h"

const int STATUS_IDLE = 0;
const int STATUS_READ = 1;
const int STATUS_WRITE_INC = 2;
const int STATUS_WRITE_ZEROS = 3;
const int STATUS_WRITE_FULL = 4;

const int PIN_CS = 4;
const int PIN_OE = 5;
const int PIN_WE = 33;

const int PIN_LATCH = 32;
const int PIN_CLOCK = 25;
const int PIN_DATA = 27;

const int PIN_BIT_0 = 16;
const int PIN_BIT_1 = 17;
const int PIN_BIT_2 = 21;
const int PIN_BIT_3 = 22;
const int PIN_BIT_4 = 23;
const int PIN_BIT_5 = 19;
const int PIN_BIT_6 = 18;
const int PIN_BIT_7 = 26;

const int RAM_SIZE = 0x0800;

const char* ssid     = "ssid";
const char* password = "pwd";

AsyncWebServer server(80);

uint8_t buffer[RAM_SIZE];

volatile uint8_t semaphore = 0;

PROGMEM uint16_t addr = -1;

PROGMEM uint8_t status = STATUS_IDLE;

PROGMEM uint8_t write_data = 0;

hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

void handleRoot(AsyncWebServerRequest *request) {
  String message = "<html><head></head><body style='font-family: sans-serif; font-size: 12px'>Following functions are available:<br><br>";
  message += "<a href='/writeinc'>write incremented data</a><br>";
  message += "<a href='/writezeros'>write all bits zero</a><br>";
  message += "<a href='/writefull'>write all bits set</a><br>";
  message += "<a href='/read'>read to buffer</a><br>";
  message += "<a href='/buffer'>get buffer</a><br>";
  request->send(200, "text/html", message);
}

void handleNotFound(AsyncWebServerRequest *request){
    request->send(404, "text/plain", "Not found");
}

void IRAM_ATTR on_time() {
  portENTER_CRITICAL_ISR(&timerMux);
  semaphore++;
  portEXIT_CRITICAL_ISR(&timerMux);
}

void init_timer(uint64_t microseconds) {
  // 80000000 / 80 = 1000000 tics / second
  timer = timerBegin(0, 80, true);                
  timerAttachInterrupt(timer, &on_time, true);    
  
  timerAlarmWrite(timer, microseconds, true);           
  timerAlarmEnable(timer);
}

inline void init_read() {
  pinMode(PIN_BIT_0, INPUT);
  pinMode(PIN_BIT_1, INPUT);
  pinMode(PIN_BIT_2, INPUT);
  pinMode(PIN_BIT_3, INPUT);
  pinMode(PIN_BIT_4, INPUT);
  pinMode(PIN_BIT_5, INPUT);
  pinMode(PIN_BIT_6, INPUT);
  pinMode(PIN_BIT_7, INPUT);
  addr = 0;
}

inline void init_write() {
  pinMode(PIN_BIT_0, OUTPUT);
  pinMode(PIN_BIT_1, OUTPUT);
  pinMode(PIN_BIT_2, OUTPUT);
  pinMode(PIN_BIT_3, OUTPUT);
  pinMode(PIN_BIT_4, OUTPUT);
  pinMode(PIN_BIT_5, OUTPUT);
  pinMode(PIN_BIT_6, OUTPUT);
  pinMode(PIN_BIT_7, OUTPUT);
  addr = 0;
  write_data = 0;
}

inline uint8_t read_input() {
  uint8_t input = B00000000;

  if (digitalRead(PIN_BIT_0) == HIGH) {
    input = input | B00000001;
  }
  if (digitalRead(PIN_BIT_1) == HIGH) {
    input = input | B00000010;
  }
  if (digitalRead(PIN_BIT_2) == HIGH) {
    input = input | B00000100;
  }
  if (digitalRead(PIN_BIT_3) == HIGH) {
    input = input | B00001000;
  }
  if (digitalRead(PIN_BIT_4) == HIGH) {
    input = input | B00010000;
  }
  if (digitalRead(PIN_BIT_5) == HIGH) {
    input = input | B00100000;
  }
  if (digitalRead(PIN_BIT_6) == HIGH) {
    input = input | B01000000;
  }
  if (digitalRead(PIN_BIT_7) == HIGH) {
    input = input | B10000000;
  }

  return input;
}

inline void write_output(uint8_t output) {
  uint8_t val = 0;
  val = (output & B00000001) == 0 ? LOW : HIGH;
  digitalWrite(PIN_BIT_0, val);
  val = (output & B00000010) == 0 ? LOW : HIGH;
  digitalWrite(PIN_BIT_1, val);
  val = (output & B00000100) == 0 ? LOW : HIGH;
  digitalWrite(PIN_BIT_2, val);
  val = (output & B00001000) == 0 ? LOW : HIGH;
  digitalWrite(PIN_BIT_3, val);
  val = (output & B00010000) == 0 ? LOW : HIGH;
  digitalWrite(PIN_BIT_4, val);
  val = (output & B00100000) == 0 ? LOW : HIGH;
  digitalWrite(PIN_BIT_5, val);
  val = (output & B01000000) == 0 ? LOW : HIGH;
  digitalWrite(PIN_BIT_6, val);
  val = (output & B10000000) == 0 ? LOW : HIGH;
  digitalWrite(PIN_BIT_7, val);
}

void setup() {
  Serial.begin(115200);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  delay(500);
  int intRetry = 0;
  while ((WiFi.status() != WL_CONNECTED) && (intRetry < 20)){
    Serial.print(".");
    intRetry++;
    delay(500);
  }

  if (WiFi.status() == WL_CONNECTED) {
    Serial.print("IP Address: ");
    Serial.println(WiFi.localIP());
  
    server.on("/", handleRoot);
    server.on("/writeinc", [](AsyncWebServerRequest *request) {
      Serial.println("handle /writeinc");
      if (status == STATUS_IDLE) {
        request->send(200, "text/plain", "ok");
        status = STATUS_WRITE_INC;
        init_write();
      }
      else {
        request->send(406, "text/plain", "active");
      }
    });
    server.on("/writezeros", [](AsyncWebServerRequest *request) {
      Serial.println("handle /writezeros");
      if (status == STATUS_IDLE) {
        request->send(200, "text/plain", "ok");
        status = STATUS_WRITE_ZEROS;
        init_write();
        write_data = 0x00;
      }
      else {
        request->send(406, "text/plain", "active");
      }
    });
    server.on("/writefull", [](AsyncWebServerRequest *request) {
      Serial.println("handle /writefull");
      if (status == STATUS_IDLE) {
        request->send(200, "text/plain", "ok");
        status = STATUS_WRITE_FULL;
        init_write();
        write_data = 0xFF;
      }
      else {
        request->send(406, "text/plain", "active");
      }
    });
    server.on("/read", [](AsyncWebServerRequest *request) {
      Serial.println("handle /read");
      if (status == STATUS_IDLE) {
        request->send(200, "text/plain", "ok");
        status = STATUS_READ;
        init_read();
      }
      else {
        request->send(406, "text/plain", "active");
      }
    });
  
    server.on("/buffer", [](AsyncWebServerRequest *request){
      Serial.println("handle /buffer");
      if (status != STATUS_IDLE) {
        request->send(406, "text/plain", "active");
      }
      request->send_P(200, "application/octet-stream", buffer, RAM_SIZE, NULL);
    });
    
    server.onNotFound(handleNotFound);
    server.begin();
  }

  init_timer(10);
  pinMode(PIN_CS, OUTPUT);
  pinMode(PIN_OE, OUTPUT);
  pinMode(PIN_WE, OUTPUT);
  digitalWrite(PIN_CS, HIGH);
  digitalWrite(PIN_OE, HIGH);
  digitalWrite(PIN_WE, HIGH);

  pinMode(PIN_LATCH, OUTPUT);
  pinMode(PIN_CLOCK, OUTPUT);
  pinMode(PIN_DATA, OUTPUT);
}

void read() {
  if (addr == 0) {
    Serial.println("Start reading...");
  }
  uint8_t low = addr & 0x00FF;
  uint8_t high = (addr & 0xFF00) >> 8;

  digitalWrite(PIN_LATCH, LOW);
  shiftOut(PIN_DATA, PIN_CLOCK, MSBFIRST, high);
  shiftOut(PIN_DATA, PIN_CLOCK, MSBFIRST, low);

  digitalWrite(PIN_CS, LOW);
  delayMicroseconds(1);
  digitalWrite(PIN_OE, LOW);
  delayMicroseconds(1);
 
  digitalWrite(PIN_LATCH, HIGH);

  delayMicroseconds(10);

  uint8_t data = read_input();
  buffer[addr] = data;
  addr++;

  digitalWrite(PIN_CS, HIGH);
  delayMicroseconds(1);
  digitalWrite(PIN_OE, HIGH);

  if (addr == RAM_SIZE) {
    addr = -1;
    status = STATUS_IDLE;
    Serial.println("...reading done!");
  }
}

void write() {
  if (addr == 0) {
    Serial.println("Start writing...");
  }
  uint8_t low = addr & 0x00FF;
  uint8_t high = (addr & 0xFF00) >> 8;

  digitalWrite(PIN_LATCH, LOW);
  shiftOut(PIN_DATA, PIN_CLOCK, MSBFIRST, high);
  shiftOut(PIN_DATA, PIN_CLOCK, MSBFIRST, low);

  digitalWrite(PIN_CS, LOW);
  digitalWrite(PIN_WE, LOW);
  delayMicroseconds(1);
 
  digitalWrite(PIN_LATCH, HIGH);

  if (status == STATUS_WRITE_INC) {
    write_data++;
  }

  write_output(write_data);
  addr++;

  digitalWrite(PIN_CS, HIGH);
  digitalWrite(PIN_WE, HIGH);

  if (addr == RAM_SIZE) {
    addr = -1;
    status = STATUS_IDLE;
    Serial.println("...writing done!");
  }
}

void loop() {
  if (status != STATUS_IDLE) {
    if (semaphore > 0) {
      portENTER_CRITICAL(&timerMux);
      semaphore--;
      portEXIT_CRITICAL(&timerMux);
      if (status == STATUS_READ) {
        read();
      }
      else {
        write();
      }
    }
  }
}
 